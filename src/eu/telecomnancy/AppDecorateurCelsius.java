package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurCelsius;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateurCelsius {
	
    public static void main(String[] args) {
        final ISensor sensor = new TemperatureSensor();
        final DecorateurCelsius sensorDeco = new DecorateurCelsius(sensor);
        new ConsoleUI(sensorDeco);
    }
    
}
