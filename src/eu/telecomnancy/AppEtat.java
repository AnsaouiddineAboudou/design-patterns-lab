package eu.telecomnancy;

import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.NewTemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppEtat {
    public static void main(String[] args) {
        ISensor sensor1 = new NewTemperatureSensor();
        new ConsoleUI(sensor1);
    }
}
