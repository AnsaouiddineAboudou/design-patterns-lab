package eu.telecomnancy.ui;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.SensorNotActivatedException;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.Properties;

public class SensorView extends JPanel implements Observer{
    private ISensor sensor;
    
    private ArrayList<JMenuItem> commandesList = new ArrayList<JMenuItem>();
    
    private Commande commandeOn;
    private Commande commandeOff;
    private Commande commandeUpdate;
    private Commande commandeGetValue;
    
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(ISensor c) throws IOException {
    	LireCommandes();

        this.sensor = c;
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(commandeOn != null) {
                    commandeOn.executer();
                }
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(commandeOff != null) {
                    commandeOff.executer();
                }
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(commandeUpdate != null) {
                    commandeUpdate.executer();
                }
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }
    
    public ArrayList<JMenuItem> getCommandList(){
    	return commandesList;
    }
    
    public void LireCommandes() throws IOException{

        ReadPropertyFile rp = new ReadPropertyFile();
        Properties p = null;
        try {
            p=rp.readFile("/eu/telecomnancy/commande.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (String i: p.stringPropertyNames()) {
        	JMenuItem item = new JMenuItem(p.getProperty(i));
            commandesList.add(item);
            if(i.equals("commandeOn")){
	            item.addActionListener(new ActionListener() {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                    if(commandeOn != null) {
	                        new CommandeOn(sensor).executer();
	                    }
	                }
	            });
            }else if(i.equals("commandeOff")){
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if(commandeOff != null) {
                            commandeOff.executer();
                        }
                    }
                });
	        }else if(i.equals("commandeUpdate")){
	            item.addActionListener(new ActionListener() {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                    if(commandeUpdate != null) {
	                        commandeUpdate.executer();
	                    }
	                }
	            });
            }else if(i.equals("commandeGetValue")){
	            item.addActionListener(new ActionListener() {
	                @Override
	                public void actionPerformed(ActionEvent e) {
	                    if(commandeGetValue != null) {
	                    	commandeGetValue.executer();
	                    }
	                }
	            });
            }
        
        }
        
    }
    
	@Override
	public void update(Observable obs, Object obj) {
		// TODO Auto-generated method stub
        if(commandeGetValue != null) {
            commandeGetValue.executer();
        }
	}
	
	public interface Commande {
	    public void executer();
	}
	
	public class CommandeOn implements Commande {
	    private ISensor sensor;
	    
	    public CommandeOn(ISensor sensor) {
	        this.sensor = sensor;
	    }
	    
	    public void executer() {
	        sensor.on();
	    }
	}
	
	public class CommandeOff implements Commande {
	    private ISensor sensor;
	    
	    public CommandeOff(ISensor sensor) {
	        this.sensor = sensor;
	    }
	    
	    public void executer() {
	        sensor.off();
	    }
	}
	
	public class CommandeUpdate implements Commande {
	    private ISensor sensor;
	    
	    public CommandeUpdate(ISensor sensor) {
	        this.sensor = sensor;
	    }
	    
	    public void executer() {
            try {
                sensor.update();
            } catch (SensorNotActivatedException sensorNotActivatedException) {
                sensorNotActivatedException.printStackTrace();
            }
	    }
	}
	
	public class CommandeGetValue implements Commande {

	    private ISensor sensor;
	    
	    public CommandeGetValue(ISensor sensor) {
	        this.sensor = sensor;
	    }
	    
	    public void executer() {
			try {
				value.setText(String.valueOf(sensor.getValue()));
			} catch (SensorNotActivatedException e) {
				e.printStackTrace();
			}
	    }
	}
	
    public void setCommandeOn(Commande commandeOn) {
        this.commandeOn = commandeOn;
    }
    
    public void setCommandeOff(Commande commandeOff) {
        this.commandeOff = commandeOff;
    }
    
    public void setCommandeUpdate(Commande commandeUpdate) {
        this.commandeUpdate = commandeUpdate;
    }
    
    public void setCommandeGetValue(Commande commandeGetValue) {
        this.commandeGetValue = commandeGetValue;
    }
}
