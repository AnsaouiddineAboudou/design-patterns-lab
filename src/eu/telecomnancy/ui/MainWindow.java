package eu.telecomnancy.ui;

import eu.telecomnancy.helpers.ReadPropertyFile;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.SensorView.Commande;

import javax.swing.*;

import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class MainWindow extends JFrame {

    private ISensor sensor;
    private SensorView sensorView;

    private JMenuBar menuBar = new JMenuBar();
    private JMenu menu = new JMenu("Menu");
    
    public MainWindow(ISensor sensor) throws IOException {
        this.sensor = sensor;
        this.sensorView = new SensorView(this.sensor);
        this.sensor.addObserver(this.sensorView);
        this.setLayout(new BorderLayout());
        this.add(this.sensorView, BorderLayout.CENTER);
        Commande commandeOn = sensorView.new CommandeOn(sensor);
        Commande commandeOff = sensorView.new CommandeOff(sensor);
        Commande commandeUpdate = sensorView.new CommandeUpdate(sensor);
        Commande commandeGetValue = sensorView.new CommandeGetValue(sensor);
        sensorView.setCommandeOff(commandeOff);
        sensorView.setCommandeOn(commandeOn);
        sensorView.setCommandeUpdate(commandeUpdate);
        sensorView.setCommandeGetValue(commandeGetValue);
        
    	for(int i=0;i<sensorView.getCommandList().size();i++){
    		this.menu.add(sensorView.getCommandList().get(i));
    	}
    	this.menuBar.add(menu);
    	this.setJMenuBar(menuBar);
        
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
    }

}
