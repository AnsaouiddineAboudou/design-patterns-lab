package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public class EtatNewTemperatureSensorOff implements EtatNewTemperatureSensor{
	private final NewTemperatureSensor newTmp;
	
	public EtatNewTemperatureSensorOff(NewTemperatureSensor newTmp){
        if (null == newTmp) {
            throw new IllegalArgumentException("L'etat doit etre associe a un capteur");
	    }
	    this.newTmp = newTmp;
	}
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
		newTmp.setEtat(new EtatNewTemperatureSensorOn(newTmp));
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub	
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}

	@Override
	public void addObserver(SensorView sensorView) {
		// TODO Auto-generated method stub
		
	}

}
