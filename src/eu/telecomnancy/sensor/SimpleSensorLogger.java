package eu.telecomnancy.sensor;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: charoy
 * Date: 13/12/13
 * Time: 18:16
 */
public class SimpleSensorLogger implements SensorLogger {
	private ArrayList<Date> date = new ArrayList<Date>();
	private ArrayList<String> info = new ArrayList<String>();
	
    @Override
    public void log(LogLevel level, String message) {
        System.out.println(level.name() + " " + message);
		Date d = new Date();
		System.out.println(d.toString());
		//On stock les infos et la date
		info.add(message);
		date.add(d);
    }
}
