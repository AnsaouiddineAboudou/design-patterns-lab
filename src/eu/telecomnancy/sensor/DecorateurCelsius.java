package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public class DecorateurCelsius extends Decorateur{

	public DecorateurCelsius(ISensor sensor) {
		super(sensor);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void on() {
		// TODO Auto-generated method stub
		sensor.on();
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		sensor.off();
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return sensor.getStatus();
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		sensor.update();
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		return ((sensor.getValue() - 32)/1.8);
	}

	@Override
	public void addObserver(SensorView sensorView) {
		// TODO Auto-generated method stub
		
	}

}
