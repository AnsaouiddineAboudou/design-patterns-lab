package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public interface EtatNewTemperatureSensor {
	
	public void on(); 
	
	public void off();

	public boolean getStatus();
	
	public void update() throws SensorNotActivatedException ;

	public double getValue() throws SensorNotActivatedException ;

	public void addObserver(SensorView sensorView);
}
