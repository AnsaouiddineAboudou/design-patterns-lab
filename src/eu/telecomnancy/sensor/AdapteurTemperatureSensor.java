package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public class AdapteurTemperatureSensor extends LegacyTemperatureSensor implements ISensor {
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
        if (!super.getStatus()){
        	onOff();
        }
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
        if (super.getStatus()){
        	onOff();
        }
	}

	@Override
	public void update() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
        if (super.getStatus()){
        	onOff();
        	onOff();
	}
        else throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
	}

	@Override
	public double getValue() throws SensorNotActivatedException {
		// TODO Auto-generated method stub
		 if (super.getStatus())
	            return getTemperature();
	     else throw new SensorNotActivatedException("Sensor must be activated to get its value.");
	}
	

	@Override
	public void addObserver(SensorView sensorView) {
		// TODO Auto-generated method stub
		
	}
	
}
