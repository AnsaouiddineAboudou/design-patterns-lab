package eu.telecomnancy.sensor;

import eu.telecomnancy.ui.SensorView;

public class NewTemperatureSensor implements ISensor {
    double value = 0;
    private EtatNewTemperatureSensor etat;
    
    public NewTemperatureSensor(){
    	etat = new EtatNewTemperatureSensorOff(this);
    }
    
    @Override
    public void on() {
        etat.on();
    }

    @Override
    public void off() {
        etat.off();
    }

    @Override
    public boolean getStatus() {
        return etat.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
    	etat.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
    	return etat.getValue();
    }

    public void setEtat(EtatNewTemperatureSensor etat){
        this.etat = etat;
    }
    
	@Override
	public void addObserver(SensorView sensorView) {
		// TODO Auto-generated method stub
		
	}
}
