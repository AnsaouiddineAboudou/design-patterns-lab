package eu.telecomnancy.sensor;

import java.util.Random;

import eu.telecomnancy.ui.SensorView;

public class EtatNewTemperatureSensorOn implements EtatNewTemperatureSensor{
	private final NewTemperatureSensor newTmp;
	
	public EtatNewTemperatureSensorOn(NewTemperatureSensor newTmp){
        if (null == newTmp) {
            throw new IllegalArgumentException("L'etat doit etre associe a un capteur");
	    }
	    this.newTmp = newTmp;
	}
	
	@Override
	public void on() {
		// TODO Auto-generated method stub
	}

	@Override
	public void off() {
		// TODO Auto-generated method stub
		newTmp.setEtat(new EtatNewTemperatureSensorOff(newTmp));
	}

	@Override
	public boolean getStatus() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
            newTmp.value = (new Random()).nextDouble() * 100;
	}

	@Override
	public double getValue() {
		// TODO Auto-generated method stub
		return newTmp.value;
	}

	@Override
	public void addObserver(SensorView sensorView) {
		// TODO Auto-generated method stub
		
	}

}
