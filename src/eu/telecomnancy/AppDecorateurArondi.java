package eu.telecomnancy;

import eu.telecomnancy.sensor.DecorateurArondi;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.sensor.TemperatureSensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppDecorateurArondi {

    public static void main(String[] args) {
        final ISensor sensor = new TemperatureSensor();
        final DecorateurArondi sensorDeco = new DecorateurArondi(sensor);
        new ConsoleUI(sensorDeco);
    }
	
}
