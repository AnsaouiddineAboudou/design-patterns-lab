package eu.telecomnancy;

import eu.telecomnancy.sensor.AdapteurTemperatureSensor;
import eu.telecomnancy.sensor.ISensor;
import eu.telecomnancy.ui.ConsoleUI;

public class AppAdaptateur {

    public static void main(String[] args) {
        ISensor sensor = new AdapteurTemperatureSensor();
        new ConsoleUI(sensor);
    }

}
